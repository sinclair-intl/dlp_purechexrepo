﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DLP_PureChex
{
    public partial class PureChexForm : Form
    {
        DbCommunication dbComm = new DbCommunication();
        RestClient restClient = new RestClient();
        int countLim = 100;
        public PureChexForm()
        {
            InitializeComponent();
            restClient.skipFlag = true;

            do
            {
                txtBoxDisplay.Text = restClient.Request("dlp_purechex_read_1", countLim) + "\n";
                if(!restClient.skipFlag)
                    break;
            } while (dbComm.countCheck("dlp_purechex_read_1", countLim));

            restClient.skipFlag = true;

            do
            {
                txtBoxDisplay.Text += restClient.Request("dlp_purechex_read_2", countLim) + "\n";
                if(!restClient.skipFlag)
                    break;
            } while (dbComm.countCheck("dlp_purechex_read_2", countLim));

            restClient.skipFlag = true;

        }

        private void PureChexForm_Load(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
