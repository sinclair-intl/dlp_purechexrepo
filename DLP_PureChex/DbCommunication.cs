﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLP_PureChex
{
    public class DbCommunication
    {
        public string dbURL { get; set; }
        public string apiKey { get; set; }
        public string id { get; set; }
        public string userID { get; set; }

        MySqlConnection connection;

        public DbCommunication()
        {

        }

        public void getVariables()
        {
            try
            {
                //first check if the state is closed before opening?

                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand command = connection.CreateCommand();
                    command.CommandText = "Select * from dlp_purechex_details where use_details = 1 ORDER BY table_id ASC LIMIT 1"; //can handle 1000 
                    try
                    {
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            dbURL = (reader["URL"].ToString());
                            apiKey = (reader["apiKey"].ToString());
                            id = (reader["id_purechex"].ToString());
                            userID = (reader["userID"].ToString());

                        }
                        //MessageBox.Show("done");
                    }
                    catch (Exception ex)
                    {
                        DbLog("Error getting the site details. Error:" + ex.Message);
                    }


                }

                connection.Close();
            }
            catch (Exception ex)
            {
                DbLog("Error establishing db connection: " + ex.Message);
            }

        }
        public void DbLog(string strLog)
        {
            try
            {
                string strFormat1 = strLog.Replace("\\", "");
                string strEntry = strFormat1.Replace("\"", "\\\"");

                MySqlConnection connection;
                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO dlp_log (message, blockchain, datetime) VALUES (\"" + strEntry.ToString() + "\", \"" + "PureChex" + "\", now())";
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        //txtBoxDisplay.Text = ("Error: " + ex.Message);
                    }
                }

                connection.Close();

            }
            catch (Exception ex)
            {
                //txtBoxDisplay.Text = ("Error: " + ex.Message);

            }
        }

        public List<string> uidList(string tableName, int countLim)
        {
            List<string> uids = new List<string>();
            try
            {
                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand command = connection.CreateCommand();
                    command.CommandText = "Select * from " + tableName + " where sent=0 ORDER BY labelling_date ASC LIMIT " + countLim; //can handle 1000 

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        uids.Add(reader["code_id"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                DbLog("Error retrieving list of codes: " + ex.Message);
            }
            connection.Close();

            return uids;
        }

        public bool markSent(string strTable, List<string> uidList)
        {
            //Just using the flag for logs record keeping
            bool bFlag = true;
            foreach (string uid in uidList)
            {
                if (!sentCodes(strTable, uid))
                    bFlag = false;
            }
            return bFlag;
        }

        public bool sentCodes(string strTable, string strUID)
        {
            try
            {
                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand command = connection.CreateCommand();
                    command.CommandText = "Update " + strTable + "  SET sent=1, uploadTime = now(), responseTime = now() where code_id = \"" + strUID + "\"";
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        DbLog("Updating db sent list error: " + ex.Message);
                    }
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                DbLog("Error establishing db connection: " + ex.Message);
                return false;
            }
            return true;
        }

        public bool countCheck(string strTable, int countLim)
        {

            try
            {
                connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=Inc0rrect!;Database=dlp_testv1.0");
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "Select COUNT(*) as count FROM " + strTable + " where sent = 0";

                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (Convert.ToDouble(reader["count"]) >= countLim)
                    {
                        reader.Close();
                        connection.Close();
                        return true;
                    }

                }
                connection.Close();
            }
            catch (Exception ex)
            {
                DbLog(ex.Message);
                return false;
            }
            return false;
        }
    }
}
