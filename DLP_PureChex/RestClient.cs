﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DLP_PureChex
{
    public class RestClient
    {
        DbCommunication dbComm = new DbCommunication();
        public string strResponse = "";

        public bool skipFlag { get; set; }

        public RestClient()
        {
            getVariables();
        }

        public void getVariables()
        {
            dbComm.getVariables();
        }

        public string Request(string strTable, int countLim)
        {

            List<string> uidList = dbComm.uidList(strTable, countLim);

            if (!uidList.Any())
            {
                return "No new codes to upload";
            }

            string postJSON = strJsonPost(strTable, uidList);
            string strResponseValue = string.Empty;
             
            HttpWebResponse response = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(dbComm.dbURL.ToString());


            request.Method = "POST";
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            request.Headers.Add("apikey", dbComm.apiKey);

            try
            {
                if (postJSON != string.Empty)
                {
                    request.ContentType = "application/json";
                    using (StreamWriter swJSONPayload = new StreamWriter(request.GetRequestStream()))
                    {
                        swJSONPayload.Write(postJSON);
                        swJSONPayload.Close();
                    }
                }

                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException("Error code: " + response.StatusCode.ToString());
                }

                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponseValue = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dbComm.DbLog("Communication/Conncection Error: " + ex.Message + "\nRequest Message: " + postJSON);
                skipFlag = false;
                return "Communication/Conncection Error: " + ex.Message;
            }
            finally
            {
                if (response != null)
                {
                    ((IDisposable)response).Dispose();
                }
            }
            dbComm.DbLog("Request: " + postJSON + "\nResponse: " + strResponseValue);

            if (resultParser(strResponseValue))
            {
                if (!dbComm.markSent(strTable, uidList))
                {
                    skipFlag = false;
                    dbComm.DbLog("Error in db update. JSon response" + strResponseValue);
                    return "Error updating the sent db";
                }
                else
                {
                    dbComm.DbLog("Successful database update");
                    return "Successful database update";
                }
            }
            else
            {
                skipFlag = false;
                return "Error parsing the response";
            }

            return strResponse;
        }



        public string strJsonPost(string strTableName, List<string> codeList)
        {
            string strLat, strLong;

            if (strTableName == "dlp_purechex_read_1")
            {
                strLat = "36.77170201295279";
                strLong = "-119.79182368341006";
            }
            else
            {
                strLat = "52.6423519";
                strLong = "1.2277914";
            }

            string strDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            //strDateTime = strDateTime.Replace("/", "");
            string postJSON = "{" +

                "\"tinyURL\":";
                foreach (string item in codeList)
            {
                postJSON += "\"" + item + "\"" + ",";
            }

            postJSON += "\"latitude\": "+ strLat +"," +
                "\"longitude\": " + strLong+ "," +
                "\"datetime\": \"" + strDateTime + "\"," +
                "\"user\": \"3100860327\"}";

            
            //postJSON = postJSON.Substring(0, postJSON.Length - 1);

            //postJSON += "]}";

            return postJSON;
        }

        public bool resultParser(string strResponse)
        {
            try
            {
                string[] strJsonFull = strResponse.Split('{');
                string[] strJsonElement = strJsonFull[1].Split(',');
                foreach (string elements in strJsonElement)
                {
                    string[] strResult = elements.Split(':');
                    if (strResult[0] == "\"status\"")
                    {

                        //return true;
                        if (strResult[1] == "true")
                        {
                            return true;
                        }
                        else
                        {
                            dbComm.DbLog("Rejection response. Response: " + strResponse);
                            return false;
                        }
                    }
                }
                dbComm.DbLog("Communication Error. Response: " + strResponse);
                return false;

            }
            catch (Exception ex)
            {
                dbComm.DbLog("Error parsing response. Response: " + strResponse);
                return false;
            }

        }
    }
}
